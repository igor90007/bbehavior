import {Component,OnDestroy,OnInit} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {Subject,Subscription} from "rxjs";
import {MeteorObservable} from 'meteor-rxjs';
import {Child} from "../../../../both/models/children.model";
import {Children} from "../../../../both/collections/children.collection";
import {Charts} from "../../../../both/collections/charts.collection";
import {InjectUser} from "angular2-meteor-accounts-ui";
import {MdSnackBar} from '@angular/material';
import template from './children.component.html';
import style from './children.component.scss';
@InjectUser("user")
@Component({
  selector:'children-demo',
  template,
  styles:[style]
})
export class ChildrenDemoComponent implements OnInit,OnDestroy{
  kate:Child;
  justin:Child;
  childrenIdsSubject:Subject<string[]>=new Subject<string[]>();
  childrenIdsArray:string[];
  children:Child[];
  childrenBhvrs:{}={};
  refreshLabel:string="Yesterday";
  childrenSub:Subscription;
  chartSub:Subscription;
  constructor(private formBuilder:FormBuilder,public snackBar:MdSnackBar){}
  ngOnInit(){
    if(this.childrenSub) {
      this.childrenSub.unsubscribe();
    };
    this.childrenSub=MeteorObservable.subscribe('childrenDemo').subscribe(()=>{
        this.kate=Children.findOne({name:"Kate"});
        this.justin=Children.findOne({name:"Justin"});
        let temporaryArray:string[]=[];
        temporaryArray.push(this.kate._id);
        temporaryArray.push(this.justin._id);
        this.childrenIdsArray=temporaryArray;
        this.childrenIdsSubject.next(this.childrenIdsArray);
        this.children=Children.find({}).fetch();
    });
    if(this.chartSub){
      this.chartSub.unsubscribe();
    };
    this.childrenIdsSubject.subscribe((childrenIdsArray)=>{
        this.chartSub=MeteorObservable.subscribe('twoDaysChartsDemo',childrenIdsArray).subscribe(()=>{
          MeteorObservable.autorun().subscribe(()=>{
              let temporaryObject:{}={};
              childrenIdsArray.forEach(value =>{
                let temporaryCharts=Charts.find({child:value}).fetch();
                let temporaryArray=[];
                temporaryCharts.forEach(value2 =>{
                  temporaryArray.push(value2.behaviors);
                  temporaryObject[value]=temporaryArray;
                })
              });
              this.childrenBhvrs=temporaryObject;
          })
        });
    })
  }
  trackByFn(index,item){
    return item._id;
  }
  save(behaviorIndex,childId){
    this.childrenBhvrs[childId][1][behaviorIndex]=this.childrenBhvrs[childId][1][behaviorIndex]+1;
    if(this.refreshLabel=="Yesterday "){
      this.refreshLabel="Yesterday";
    }else{
      this.refreshLabel="Yesterday ";
    }
  }
  ngOnDestroy(){
    this.childrenSub.unsubscribe();
    this.chartSub.unsubscribe();
  }
}