import {Component,OnInit,OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {Subject} from "rxjs";
import {MeteorObservable} from 'meteor-rxjs';
import {Child} from "../../../../both/models/children.model";
import {Children} from "../../../../both/collections/children.collection";
import {Chart} from "../../../../both/models/charts.model";
import {Charts} from "../../../../both/collections/charts.collection";
import 'rxjs/add/operator/map';
import template from './children-report.component.html';
import style from './children-report.component.scss';
@Component({
    selector:'children-report',
    template,
    styles:[style]
})
export class ChildrenReportComponent implements OnInit,OnDestroy{
    name:string;
    showChart:boolean=false;
    labels:string[]=['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
    months:string[]=[];
    allBehaviors:{}={};
    paramsSub:Subscription;
    chartSub:Subscription;
    childSubs:Subscription;
    charts:Chart[];
    child:Child;
    childSubject:Subject<Child>=new Subject<Child>();
    behaviorNames:string[];
    constructor(private route:ActivatedRoute){}
    ngOnInit(){
        this.paramsSub=this.route.params
            .map(params => params['childId'])
            .subscribe(childId=>{
                if(this.childSubs){
                    this.childSubs.unsubscribe();
                };
                this.childSubs=MeteorObservable.subscribe('child',childId).subscribe(()=>{
                    this.child=Children.findOne(childId);
                    this.name=this.child.name;
                    this.childSubject.next(this.child);
                })
            });
        this.childSubject.subscribe((child)=>{
            if(this.chartSub){
                this.chartSub.unsubscribe();
            };
            this.chartSub=MeteorObservable.subscribe('charts',child._id).subscribe(()=>{
                MeteorObservable.autorun().subscribe(()=>{
                        this.charts=Charts.find({}).fetch();
                        this.charts.forEach(value=>{
                            let temporaryDate=new Date(value.date);
                            let temporaryMonth=temporaryDate.getMonth();
                            let monthName;
                            switch(temporaryMonth){
                                case 0:
                                    monthName="January";
                                    break;
                                case 1:
                                    monthName="February";
                                    break;
                                case 2:
                                    monthName="March";
                                    break;
                                case 3:
                                    monthName="April";
                                    break;
                                case 4:
                                    monthName="May";
                                    break;
                                case 5:
                                    monthName="June";
                                    break;
                                case 6:
                                    monthName="July";
                                    break;
                                case 7:
                                    monthName="August";
                                    break;
                                case 8:
                                    monthName="September";
                                    break;
                                case 9:
                                    monthName="October";
                                    break;
                                case 10:
                                    monthName="November";
                                    break;
                                default:
                                    monthName="December";
                            };
                            if(this.months.indexOf(monthName)==-1){
                                this.months.push(monthName);
                            }
                        });
                        for(var i=0;i<child.behaviorNames.length;i++){
                                this.allBehaviors[child.behaviorNames[i]]=[];
                                eval('var perem'+i+'={}');
                                for(var n=0;n<this.months.length;n++){
                                    let temporaryObject:{}={};
                                    temporaryObject["data"]=[];
                                    temporaryObject["label"]=this.months[n];
                                    eval('perem'+i+'["'+this.months[n]+'"]=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]');
                                    this.allBehaviors[child.behaviorNames[i]].push(temporaryObject);
                                }
                        };
                        for(var m=0;m<this.charts.length;m++){
                            let thisData=this.charts[m];
                            let temporaryDate=new Date(thisData.date);
                            let temporaryDay=temporaryDate.getDate()-1;
                            let temporaryMonth=temporaryDate.getMonth();
                            let monthName;
                            switch(temporaryMonth){
                                case 0:
                                    monthName="January";
                                    break;
                                case 1:
                                    monthName="February";
                                    break;
                                case 2:
                                    monthName="March";
                                    break;
                                case 3:
                                    monthName="April";
                                    break;
                                case 4:
                                    monthName="May";
                                    break;
                                case 5:
                                    monthName="June";
                                    break;
                                case 6:
                                    monthName="July";
                                    break;
                                case 7:
                                    monthName="August";
                                    break;
                                case 8:
                                    monthName="September";
                                    break;
                                case 9:
                                    monthName="October";
                                    break;
                                case 10:
                                    monthName="November";
                                    break;
                                default:
                                    monthName="December";
                            };
                            for(var l=0;l<thisData.behaviors.length;l++){
                                eval('perem'+l+'["'+monthName+'"]['+temporaryDay+']='+thisData.behaviors[l]);
                            }
                        }
                        for(var k=0;k<child.behaviorNames.length;k++){
                            for(var f=0;f<this.months.length;f++){
                                eval('var thisPerem=perem'+k);
                                this.allBehaviors[child.behaviorNames[k]][f]["data"]=thisPerem[this.months[f]];
                            }
                        }
                        this.behaviorNames=child.behaviorNames;
                        this.showChart=true;
                })
            });
        });
    }
    ngOnDestroy(){
        this.paramsSub.unsubscribe();
        this.chartSub.unsubscribe();
        this.childSubs.unsubscribe();
    }
}