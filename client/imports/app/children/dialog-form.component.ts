import {Component,OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {MeteorObservable} from 'meteor-rxjs';
import {MdSnackBar,MdDialogRef} from '@angular/material';
import {InjectUser} from "angular2-meteor-accounts-ui";
import {Children} from '../../../../both/collections/children.collection';
import {Charts} from '../../../../both/collections/charts.collection';
import {Parent} from "../../../../both/models/parent.model";
import {Parents} from '../../../../both/collections/users.collection';
import template from './dialog-form.component.html';
import style from './dialog-form.component.scss';
@Component({
    selector:'dialog-result-dialog',
    template,
    styles:[style]
})
@InjectUser("user")
export class FormResultDialog implements OnInit{
    parent:Parent;
    showReports:boolean;
    isCorrdova:boolean=Meteor.isCordova;
    addForm:FormGroup;
    addReport:FormGroup;
    image:string='';
    thumb:string='';
    demo:boolean=false;
    behaviorNames:string[]=['Disruptive','Lying','Inattention','Uncooperative'];
    constructor(private router:Router,private formBuilder:FormBuilder,public snackBar:MdSnackBar,public dialogRef:MdDialogRef<FormResultDialog>){}
    ngOnInit(){
        this.addForm=this.formBuilder.group({
            name:['',Validators.required]
        });
        this.addReport=this.formBuilder.group({
            email:['',Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]
        });
        this.parent=Parents.findOne(Meteor.userId());
        if(this.parent.children.length==0){
            this.showReports=false;
        }else{
            this.showReports=true;
        }
    }
    addChild(){
        if(!Meteor.userId()){
            this.demo=true;
        }
        if(this.addForm.valid){
            Children.insert({
                name:this.addForm.value.name,
                demo:this.demo,
                parent:Meteor.userId(),
                image:this.image,
                thumb:this.thumb,
                behaviorNames:this.behaviorNames
            });
            let temporaryChild=Children.findOne({parent:Meteor.userId(),name:this.addForm.value.name});
            Parents.update(Meteor.userId(),{
                $push:{
                    children:temporaryChild._id
                }
            });
            Charts.insert({
                child:temporaryChild._id,
                date:Date.now(),
                behaviors:[0,0,0,0]
            });
            Charts.insert({
                child:temporaryChild._id,
                date:Date.now()-86400000,
                behaviors:[0,0,0,0]
            });
            this.addForm.reset();
            this.dialogRef.close();
        }else{
            this.addForm.reset();
            this.snackBar.open("Empty ","name!",{duration:999});
        }
    }
    onImage(imageData:string[]){
        this.image=imageData[0];
        this.thumb=imageData[1];
    }
    sendReport(){
        if(this.addReport.valid){
            let tempHref:string;
            if(Meteor.userId()){
                tempHref=location.href+"child/";
            }else{
                tempHref=location.href.substr(0,location.href.length-4)+"child/";
            }
            MeteorObservable.call('sendReport',this.addReport.value.email,tempHref,this.parent.children[0]).subscribe(()=>{
                this.snackBar.open(`Report successfully sent.`,"ok",{duration:999});
                this.addReport.reset();
                this.dialogRef.close();
            },(error)=>{
                this.addReport.reset();
                this.snackBar.open(`${error}`,"<<Failed to send due to...");
                this.dialogRef.close();
            });
        }else{
            this.addReport.reset();
            this.snackBar.open("Invalid email","ok!",{duration:999});
            this.dialogRef.close();
        }
    }
    goToReports(){
        this.router.navigate(['/child',this.parent.children[0]]);
        this.dialogRef.close();
    }
}