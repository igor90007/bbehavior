import {ChildrenDemoComponent} from './children-demo.component';
import {ChildrenComponent} from './children.component';
import {FormResultDialog} from './dialog-form.component';
import {ChildrenUploadComponent} from "./children-upload.component";
import {ChildrenReportComponent} from "./children-report.component";
export const CHILDREN_DECLARATIONS=[
  ChildrenDemoComponent,
  ChildrenComponent,
  FormResultDialog,
  ChildrenUploadComponent,
  ChildrenReportComponent
];
