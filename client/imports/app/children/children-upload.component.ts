import {Component,OnInit,EventEmitter,Output} from '@angular/core';
import {Subject,Subscription} from "rxjs";
import {MeteorObservable} from "meteor-rxjs";
import template from './children-upload.component.html';
import style from './children-upload.component.scss';
import {upload} from '../../../../both/methods/images.methods';
import {Thumb} from "../../../../both/models/image.model";
import {Thumbs} from "../../../../both/collections/images.collection";
@Component({
  selector:'children-upload',
  template,
  styles:[style]
})
export class ChildrenUploadComponent implements OnInit{
  fileIsOver:boolean=false;
  uploading:boolean=false;
  eventData:string[]=[];
  file:Subject<string>=new Subject<string>();
  thumbsSubscription:Subscription;
  thumb:Thumb;
  @Output() onFile: EventEmitter<string[]> = new EventEmitter<string[]>();
  constructor(){}
  ngOnInit(){
    this.file.subscribe((fileId)=>{
      MeteorObservable.autorun().subscribe(()=>{
        if (this.thumbsSubscription){
          this.thumbsSubscription.unsubscribe();
          this.thumbsSubscription=undefined;
        }
        this.thumbsSubscription=MeteorObservable.subscribe("thumbs",fileId).subscribe(()=>{
          MeteorObservable.autorun().subscribe(()=>{
            this.thumb=Thumbs.findOne({originalStore:'images',originalId:fileId});
            if(this.thumb && this.thumb.url){
              this.eventData.push(this.thumb.url);
              this.onFile.emit(this.eventData);
            }
          })
        });
      });
    });
  }
  fileOver(fileIsOver:boolean):void{
    this.fileIsOver=fileIsOver;
  }
  onFileDrop(file:File):void{
    this.uploading=true;
    upload(file)
      .then((result)=>{
        this.uploading=false;
        this.addFile(result);
      })
      .catch((error)=>{
        this.uploading=false;
        console.log(`Something went wrong!`,error);
      });
  }
  addFile(file){
    this.eventData.push(file._id);
    this.file.next(file._id);
  }
  reset(){
    this.eventData=[];
    this.file.next("");
  }
}