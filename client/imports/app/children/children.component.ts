import {Component,OnDestroy,OnInit} from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';
import {Subject,Subscription} from "rxjs";
import {MeteorObservable} from 'meteor-rxjs';
import {InjectUser} from "angular2-meteor-accounts-ui";
import {MdSnackBar,MdDialog} from '@angular/material';
import {Charts} from "../../../../both/collections/charts.collection";
import {Child} from "../../../../both/models/children.model";
import {Children} from "../../../../both/collections/children.collection";
import {Parent} from "../../../../both/models/parent.model";
import {Parents} from "../../../../both/collections/users.collection";
import {ChildrenService} from '../services/children.service';
import {FormResultDialog} from './dialog-form.component';
import template from './children.component.html';
import style from './children.component.scss';
@InjectUser("user")
@Component({
  selector:'children',
  template,
  styles:[style]
})
export class ChildrenComponent implements OnInit,OnDestroy{
  showBoarding:boolean=false;
  charDates:{}={};
  addReport:FormGroup;
  addCategory:FormGroup;
  parent:Parent;
  children:Child[];
  childrenBhvrs:{}={};
  childrenSub:Subscription;
  chartSub:Subscription;
  usersSubs:Subscription;
  openPopupSubscription:Subscription;
  parentSubject:Subject<Parent>=new Subject<Parent>();
  refreshLabel:string="Yesterday";
  constructor(private formBuilder:FormBuilder,public snackBar:MdSnackBar,private childService:ChildrenService,public dialog:MdDialog){
    this.openPopupSubscription=childService.serviceAnnounced$.subscribe(shooldOpen=>{
          if(shooldOpen=="true"){
            this.dialog.open(FormResultDialog);
          }
    });
  }
  ngOnInit(){
    this.addCategory=this.formBuilder.group({
      category:['',Validators.required]
    });
    this.addReport=this.formBuilder.group({
      email:['',Validators.pattern('[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}')]
    });
    if(this.usersSubs) {
      this.usersSubs.unsubscribe();
    };
    this.usersSubs=MeteorObservable.subscribe('users',Meteor.userId()).subscribe(()=>{
      MeteorObservable.autorun().subscribe(()=>{
        this.parent = Parents.findOne();
        if (this.parent) {
          this.parentSubject.next(this.parent);
        } else {
          Parents.insert({
            _id: Meteor.userId(),
            children: []
          });
        }
      })
    });
    this.parentSubject.subscribe((parent)=>{
      MeteorObservable.autorun().subscribe(() => {
        this.chartSub=MeteorObservable.subscribe('twoDaysCharts',parent.children).subscribe(()=>{
          MeteorObservable.autorun().subscribe(()=>{
            let temporaryObject:{} = {};
            parent.children.forEach(value => {
              let temporaryCharts = Charts.find({child:value},{sort:{date:1}}).fetch();
              let temporaryArray = [];
              temporaryCharts.forEach(value2=>{
                this.charDates[value]=value2.date;
                temporaryArray.push(value2.behaviors);
                temporaryObject[value] = temporaryArray;
              })
            });
            this.refresh(()=>{
              this.childrenBhvrs=temporaryObject;
            });
          })
        });
        if(this.childrenSub){
          this.childrenSub.unsubscribe();
        };
        this.childrenSub = MeteorObservable.subscribe('children',parent._id).subscribe(()=>{
          MeteorObservable.autorun().subscribe(()=>{
            this.children=Children.find({}).fetch();
            if(this.children.length==0){
              this.showBoarding=true;
            }
          })
        });
      })
    });
  }
  refresh(doneCallback:()=>void){
    doneCallback();
  }
  addingCategory(child){
    if(this.addCategory.valid){
      this.childrenBhvrs[child._id][1].push(0);
      child.behaviorNames.push(this.addCategory.value.category);
      let thisChart=Charts.findOne({child:child._id,date:this.charDates[child._id]});
      Charts.update({_id:thisChart._id},{
        $set:{
          behaviors:this.childrenBhvrs[child._id][1]
        }
      });
      Children.update(child._id,{
        $set:{
          behaviorNames:child.behaviorNames
        }
      });
      this.addCategory.reset();
    }else{
      this.addCategory.reset();
      this.snackBar.open("Empty","category",{duration:999});
    }
  }
  trackByFn(index,item){
    return item._id;
  }
  save(behaviorIndex,childId){
    let dateNow=Date.now();
    if(dateNow-this.charDates[childId]>86400000){
      let temporaryBhvrs:number[]=[];
      this.childrenBhvrs[childId][1].forEach(value=>{
        temporaryBhvrs.push(0);
      });
      temporaryBhvrs[behaviorIndex]=1;
      Charts.insert({
        child:childId,
        date:Date.now(),
        behaviors:temporaryBhvrs
      });
    }else{
      this.childrenBhvrs[childId][1][behaviorIndex]+=+1;
      let thisChart=Charts.findOne({child:childId,date:this.charDates[childId]});
      Charts.update({_id:thisChart._id},{
        $set:{
          behaviors:this.childrenBhvrs[childId][1]
        }
      });
    }
  }
  offBoarding(){
    this.showBoarding=false;
  }
  ngOnDestroy(){
    this.usersSubs.unsubscribe();
    this.childrenSub.unsubscribe();
    this.chartSub.unsubscribe();
    this.openPopupSubscription.unsubscribe();
  }
}