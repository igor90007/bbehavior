import {Injectable} from '@angular/core';
import {Route,Router,CanActivate} from '@angular/router';
import {Meteor} from 'meteor/meteor';
import {ChildrenComponent} from './children/children.component';
import {ChildrenDemoComponent} from './children/children-demo.component';
import {ChildrenReportComponent} from './children/children-report.component';
@Injectable()
export class checkUser implements CanActivate{
    constructor(private router:Router){}
    canActivate():boolean{
        if(Meteor.userId()){
            return true;
        }else{
            this.router.navigate(['/demo']);
            return false;
        }
    }
}
@Injectable()
export class checkDemo implements CanActivate{
    constructor(private router:Router){}
    canActivate():boolean{
        if(Meteor.userId()){
            this.router.navigate(['/']);
            return false;
        }else{
            return true;
        }
    }
}
export const routes:Route[]=[
    {path:'',component:ChildrenComponent,canActivate:[checkUser]},
    {path:'demo',component:ChildrenDemoComponent,canActivate:[checkDemo]},
    {path:'child/:childId',component:ChildrenReportComponent}
];
