import {Component,OnDestroy} from "@angular/core";
import {Router} from '@angular/router';
import {MeteorObservable} from 'meteor-rxjs';
import {Subscription} from "rxjs";
import {ChildrenService} from './services/children.service';
import {Child} from "../../../both/models/children.model";
import {Children} from "../../../both/collections/children.collection";
import {InjectUser} from "angular2-meteor-accounts-ui";
import template from "./app.component.html";
import style from "./app.component.scss";
@Component({
  selector: "app",
  template,
  styles:[style],
  providers:[ChildrenService]
})
@InjectUser('user')
export class AppComponent implements OnDestroy{
  reportUrl:boolean=true;
  childrenSub_:Subscription;
  children:Child[];
  childSub_:Subscription;
  child:Child;
  constructor(private router:Router,private childService:ChildrenService){
    router.events.subscribe((val)=>{
      if(location.href.indexOf("child")==-1){
        this.reportUrl=false;
      }else{
          this.children=Children.find({parent:Meteor.userId()}).fetch();
          if(this.children.length==0){
            if(this.childrenSub_){
              this.childrenSub_.unsubscribe();
            };
            this.reportUrl=true;
            if(Meteor.userId()){
              this.childrenSub_=MeteorObservable.subscribe('children',Meteor.userId()).subscribe(()=>{
                this.children=Children.find({parent:Meteor.userId()}).fetch();
              });
            }else{
              let childId=location.href.substr(location.href.indexOf("child")+6);
              if(this.childSub_){
                this.childSub_.unsubscribe();
              };
              this.childSub_=MeteorObservable.subscribe('child',childId).subscribe(()=>{
                this.child=Children.findOne(childId);
                this.childrenSub_=MeteorObservable.subscribe('children',this.child.parent).subscribe(()=>{
                  this.children=Children.find({parent:this.child.parent}).fetch();
                });
              });
            }
          }else{
            this.reportUrl=true;
          };
      }
    });
    MeteorObservable.autorun().subscribe(()=>{
      if(Meteor.userId()){
        if(location.href.indexOf("child")==-1){
          this.router.navigate(['/']);
          return;
        }else{
          return;
        }
      }else{
        this.router.navigate(['/demo']);
        return;
      }
    })
  }
  goToChart(childId){
    this.router.navigate(['/child',childId]);
  }
  openAddChildForm(){
    this.childService.announceService("true");
  }
  ngOnDestroy(){
    this.childrenSub_.unsubscribe();
    this.childSub_.unsubscribe();
  }
}
