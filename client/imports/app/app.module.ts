import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from "./app.component";
import {FormResultDialog} from "./children/dialog-form.component";
import {RouterModule} from '@angular/router';
import {AccountsModule} from 'angular2-meteor-accounts-ui';
import {routes,checkUser,checkDemo} from './app.routes';
import {MaterialModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FileDropModule} from "angular2-file-drop";
import {ChartsModule} from 'ng2-charts';
import {CHILDREN_DECLARATIONS} from './children';
import {SHARED_DECLARATIONS} from './shared';
let moduleDefinition;
moduleDefinition={
    imports:[
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes),
        AccountsModule,
        MaterialModule,
        FlexLayoutModule.forRoot(),
        FileDropModule,
        ChartsModule
    ],
    declarations:[
        AppComponent,
        ...CHILDREN_DECLARATIONS,
        ...SHARED_DECLARATIONS
    ],
    providers:[
        checkUser,
        checkDemo
    ],
    bootstrap:[
      AppComponent
    ],
    entryComponents:[
        FormResultDialog
    ]
}
@NgModule(moduleDefinition)
export class AppModule{}