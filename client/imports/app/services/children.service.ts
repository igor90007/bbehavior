import {Injectable} from '@angular/core';
import {Subject}    from 'rxjs/Subject';
@Injectable()
export class ChildrenService{
    private serviceAnnouncedSource=new Subject<string>();
    serviceAnnounced$=this.serviceAnnouncedSource.asObservable();
    announceService(doIt:string){
        this.serviceAnnouncedSource.next(doIt);
    }
}