import {Email} from 'meteor/email';
import {check} from 'meteor/check';
import {Meteor} from 'meteor/meteor';
function getContactEmail(user:Meteor.User):string{
  if(user && user.emails && user.emails.length)
    return user.emails[0].address;

  return null;
}
Meteor.methods({
  sendReport:function(email:string,locationHref:string,child:string){
    check(child,String);
    let from=getContactEmail(Meteor.users.findOne(Meteor.userId()));
    if(Meteor.isServer){
        Email.send({
          from:'noreply@behavior.com',
          to:email,
          replyTo:from || undefined,
          subject:'Reports about '+from+' `s children',
          text:`Link to report page:
            \n${locationHref}${child}`
        });
    }
  }
});