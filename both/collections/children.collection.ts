import {MongoObservable} from 'meteor-rxjs';
import {Meteor} from 'meteor/meteor';
import {Child} from '../models/children.model';
export const Children=new MongoObservable.Collection<Child>('children');
function loggedIn() {
  return !!Meteor.user();
}
Children.allow({
  insert:loggedIn,
  update:loggedIn,
  remove:loggedIn
});
