import {MongoObservable} from 'meteor-rxjs';
import {Meteor} from 'meteor/meteor';
import {Chart} from '../models/charts.model';
export const Charts=new MongoObservable.Collection<Chart>('charts');
function loggedIn(){
  return !!Meteor.user();
}
Charts.allow({
  insert:loggedIn,
  update:loggedIn,
  remove:loggedIn
});
