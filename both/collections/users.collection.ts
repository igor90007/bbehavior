import {MongoObservable} from 'meteor-rxjs';
import {Meteor} from 'meteor/meteor';
import {Parent} from '../models/parent.model';
export const Parents=new MongoObservable.Collection<Parent>("parents");
function loggedIn(){
    return !!Meteor.user();
}
Parents.allow({
    insert:loggedIn,
    update:loggedIn,
    remove:loggedIn
});
