import {CollectionObject} from './collection-object.model';
export interface Parent extends CollectionObject{
    children:string[]
}
