import {CollectionObject} from './collection-object.model';
export interface Chart extends CollectionObject{
  child:string;
  date:number;
  behaviors:number[];
}