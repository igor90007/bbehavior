import {CollectionObject} from './collection-object.model';
export interface Child extends CollectionObject{
  name:string;
  demo:boolean;
  parent:string;
  image?:string;
  thumb?:string;
  behaviorNames:string[];
}