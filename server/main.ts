import {Meteor} from 'meteor/meteor';
import {loadParent} from './imports/fixtures/demoParent';
import './imports/publications/children';
import './imports/publications/images';
import './imports/publications/charts';
import './imports/publications/users';
import '../both/methods/children.methods';
Meteor.startup(() => {
  loadParent();
  process.env.MAIL_URL='smtp://igor90007:!2896557@smtp.gmail.com:465';
});
