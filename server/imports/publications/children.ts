import {Meteor} from 'meteor/meteor';
import {Children} from '../../../both/collections/children.collection';
import {Parents} from '../../../both/collections/users.collection';
Meteor.publish('children',function(parentId:string){
  const parent=Parents.findOne(parentId);
  if(!parent){
    throw new Meteor.Error('404','No such parent!');
  }
  return Children.find({parent:parentId});
});
Meteor.publish('childrenDemo',function(){
  return Children.find({demo:true});
});
Meteor.publish('child',function(childId:string){
  return Children.find(childId);
});