import {Meteor} from 'meteor/meteor';
import {Charts} from '../../../both/collections/charts.collection';
import {Children} from '../../../both/collections/children.collection';
Meteor.publish('charts',function(childId:string){
  const child=Children.findOne(childId);
  if(!child){
    throw new Meteor.Error('404','No such child!');
  }
  return Charts.find({child:childId});
});
Meteor.publish('twoDaysChartsDemo',function(childrenIds:string[]){
    if(!childrenIds){
        throw new Meteor.Error('404','No such children IDs!');
    }
    return Charts.find({child:{$in:childrenIds}},{limit:childrenIds.length*2});
});
Meteor.publish('twoDaysCharts',function(childrenIds:string[]){
    if(!childrenIds){
        throw new Meteor.Error('404','No such children IDs!');
    }
    return Charts.find({child:{$in:childrenIds},date:{$gt:Date.now()-86400000*2}});
});